#include <stdio.h>
#include "util.h"
#include <cuda_runtime.h>
using namespace std;


__global__ void matrixAdd_Grid1D_Block1D(float* M, float*N, float* R, int nx, int ny) {
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx < nx*ny) {
    R[idx] = M[idx] + N[idx];
  }
}

//__global__
//void matrixAdd_Grid1D_Block2D(float* M, float*N, float* R, int nx, int ny) {
//
//}

//__global__
//void matrixAdd_Grid2D_Block2D(float* M, float*N, float* R, int nx, int ny) {
//
//}

void matrixAddHost(float* M, float* N, float* R, int nx, int ny) {
  for (int i=0; i<nx*ny; ++i) {
    R[i] = M[i] + N[i];
  }
}


int main(int argc, char** argv) {
  srand(0);
  printf("Matrix addition on GPU\n");

  cudaSetDevice(0);
  

  int nx = argc > 1 ? atoi(argv[1]) : 8;
  int ny = argc > 2 ? atoi(argv[2]) : 6;
    
  int size = nx*ny;
  size_t nBytes = size*sizeof(float);

  // host data
  float* h_matA = new float[nBytes];
  float* h_matB = new float[nBytes];
  float* h_matC = new float[nBytes];
  float* gpuRef = new float[nBytes];

  fillArrayF(h_matA, size);
  fillArrayF(h_matB, size);

  // device data (gpu)
  float *d_matA, *d_matB, *d_matC;
  cudaMalloc((float**) &d_matA, nBytes);
  cudaMalloc((float**) &d_matB, nBytes);
  cudaMalloc((float**) &d_matC, nBytes);

  CHECK(cudaMemcpy(d_matA, h_matA, nBytes, cudaMemcpyHostToDevice));
  CHECK(cudaMemcpy(d_matB, h_matB, nBytes, cudaMemcpyHostToDevice));

  dim3 block(1024);
  dim3 grid((nx*ny + block.x-1) / block.x);
  
  matrixAdd_Grid1D_Block1D <<<grid, block>>>(d_matA, d_matB, d_matC, nx, ny);
  matrixAddHost(h_matA, h_matB, h_matC, nx, ny);

  CHECK(cudaDeviceSynchronize());

  cudaMemcpy(gpuRef, d_matC, nBytes, cudaMemcpyDeviceToHost);

  checkEqual(h_matC, gpuRef, size);
  
  delete[] h_matA;
  delete[] h_matB;
  delete[] h_matC;
  delete[] gpuRef;

  cudaFree(d_matA);
  cudaFree(d_matB);
  cudaFree(d_matC);

  cudaDeviceReset();
	     
  return 0;
}