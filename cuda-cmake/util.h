#ifndef __util_h__
#define __util_h__

#define CHECK(call)                                                         \
{                                                                           \
    const cudaError_t error = call;                                         \
    if (error != cudaSuccess)                                               \
    {                                                                       \
        printf("Error: %s:%d, ", __FILE__, __LINE__);                       \
        printf("code:%d, reason: %s\n", error, cudaGetErrorString(error));  \
        exit(1);                                                            \
    }                                                                       \
}

  void printMatrix(float* M, float* N, int size);
  void fillArrayF(float* arr, int n);
  void fillArrayI(int* arr, int n);
  bool checkEqual(float* A, float* B, int size);
  bool checkEqualI(int a, int b);

#endif //__util_h__
