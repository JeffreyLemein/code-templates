#include <stdio.h>
#include <cstdlib>
#include "util.h"

class Hello {
private:
  void howAreYou() {}
  void goodbye(){}
};

//namespace myutil {
void fillArrayI(float* arr, int n) {

  
  //random_device rd;
  //mt19937 gen(rd());
  //uniform_real_distribution<> dist(0.0,100.0);

  for (int i=0; i<n; ++i) {
    arr[i] = rand() / static_cast<float>(RAND_MAX);
  }
}

void fillArrayF(int* arr, int n) {
  //random_device rd;
  //mt19937 gen(rd());
  //uniform_real_distribution<> dist(0.0,100.0);

  for (int i=0; i<n; ++i) {
    arr[i] = rand() % 500;
  }
}


void printMatrix(float* M, float* N, int size) {
  for (int i=0; i<size; ++i) {
    printf("[%d] = %f  vs  %f\n", i, M[i], N[i]);
  }
}

bool checkEqual(float* A, float* B, int size) {
  for (int i=0; i<size; ++i) {
    if (A[i] != B[i]) {
      printf("[FAIL] Matrices are different\n");

      printf("Comparing \n");
      printMatrix(A, B, size);

      return false;
    }
  }
  
  printf("[OK] Matrices are equal\n");
  return true;
}

bool checkEqualI(int a, int b) {
  if (a != b) {
    printf("[FAIL] Values are different %d   vs   %d\n", a, b);
    return false;
  } else {
    printf("[OK] Values are equal\n");
    return true;
  }
}
//}
