#include <stdio.h>
#include "util.h"

__global__ void reduceNeighbored(int *g_idata, int *g_odata, unsigned int n) {
  unsigned int tid = threadIdx.x;
  int *idata = g_idata + blockIdx.x * blockDim.x;

  if (tid >= n)
    return;

  for (int stride = 1; stride < blockDim.x; stride *= 2) {
    if ((tid %(2*stride)) == 0) {
      idata[tid] += idata[tid + stride];
    }

    // synchronize within block
    __syncthreads();
  }

  // write result for this bock to global member
  if (tid == 0) g_odata[blockIdx.x] = idata[0];
}

int reduceNeighboredHost(int* M, int size) {
  int sum = 0;
  for (int i=0; i<size; ++i) {
    sum += M[i];
  }
  return sum;
}

void reduceNeighboredBaseline(){

  //reduceNeighbored <<< >>> (g_;
  
  //TODO: Implement
  printf("CPU reduce: elapsed x secs\n");
  printf("GPU neighbored: elapsed x secs\n");
}

int main() {
  int dev = 0;
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, dev);
  cudaSetDevice(dev);

  int size = 1<<24;

  dim3 block;
  dim3 grid;
  
  // allocate host memory
  size_t nBytes = size * sizeof(int);
  int *h_input = new int[size];
  int *h_output = new int[grid.x];

  // initialize array
  fillArrayI(h_input, size);

  int *d_input = 0, *d_output = 0;
  cudaMalloc((void **) &d_input, nBytes);
  cudaMalloc((void **) &d_output, grid.x * sizeof(int));

  reduceNeighbored <<< grid, block >>>(d_input, d_output, size);
  // cpu reduction
  int sumHost = reduceNeighboredHost(h_input, size);

  cudaDeviceSynchronize();

  cudaMemcpy(h_output, d_output, grid.x * sizeof(int), cudaMemcpyDeviceToHost);
  int sumGpu = 0;
  for (int i=0; i<grid.x; ++i) {
    sumGpu += h_output[i];
  }

  checkEqualI(sumHost, sumGpu);
  
  delete[] h_input;
  delete[] h_output;

  cudaFree(d_input);
  cudaFree(d_output);
  
  return 0;
}
