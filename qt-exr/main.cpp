#include <vector>

#include <OpenEXR/Iex.h>
#include <OpenEXR/half.h>
#include <OpenEXR/ImfRgbaFile.h>
using namespace Imf_2_2;

#include <QApplication>
#include <QPushButton>

int main(int argc, char** argv) {

    int width = 200;
    int height = 200;
    std::vector<Rgba> pixels;
    pixels.reserve(width*height);

    for (int x=0; x<width; ++x) {
        for (int y=0; y<height; ++y) {
            if (x == y) {
                pixels.push_back(Rgba(1.0, 0.0f, 0.0f));
            } else {
                pixels.push_back(Rgba(1.0, 1.0f, 0.0f));
            }
        }
    }

    RgbaOutputFile file("test.exr", width, height, WRITE_RGBA);
    file.setFrameBuffer(pixels.data(), 1, width);
    file.writePixels(height);

    QApplication app(argc, argv);
    QPushButton button("Hello World");
    button.show();

    return app.exec();
}
