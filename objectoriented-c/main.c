#include <stdio.h>
#include "integrator.h"
#include "pathIntegrator.h"
#include "volumeIntegrator.h"

int main(int argc, char **argv) {

    struct Integrator pathIntegrator = { PathIntegrator };
    struct Integrator volumeIntegrator = { VolumeIntegrator };

    printf("Hello World from C\n");

    integrator_integrate(&pathIntegrator);
    integrator_integrate(&volumeIntegrator);


    return 0;
}
