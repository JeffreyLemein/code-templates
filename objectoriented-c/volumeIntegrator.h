#ifndef _VOLUME_INTEGRATOR_H_
#define _VOLUME_INTEGRATOR_H_

#include <stdio.h>
#include "integrator.h"

static const char* VolumeIntegrator_getName() {
    return "VolumeIntegrator#1\n";
}

static void VolumeIntegrator_integrate() {
    printf("Integrate from volume integrator\n");
}

const struct Integrator_vtable_ VolumeIntegrator[] = {{ VolumeIntegrator_getName, VolumeIntegrator_integrate }};


#endif
