#ifndef _PATH_INTEGRATOR_H_
#define _PATH_INTEGRATOR_H_

#include <stdio.h>
#include "integrator.h"

static const char* PathIntegrator_getName() {
    return "PathIntegrator#1\n";
}

static void PathIntegrator_integrate() {
    printf("Integrate from path integrator\n");
}

const struct Integrator_vtable_ PathIntegrator[] = {{ PathIntegrator_getName, PathIntegrator_integrate }};


#endif
