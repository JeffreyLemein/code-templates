#ifndef ANIMAL_H_
#define ANIMAL_H_

struct Integrator
{
    const struct Integrator_vtable_ *vtable_;
};

struct Integrator_vtable_
{
    const char *(*getName)(void);
    const void *(*integrate)(void);
};

static inline void integrator_integrate(struct Integrator *integrator) {
    integrator->vtable_->integrate();
}

static inline const char *integrator_getName(struct Integrator *integrator) {
    return integrator->vtable_->getName();
}

// make the vtables arrays so they can be used as pointers
extern const struct Integrator_vtable_ VolumeIntegrator[], PathTracer[];

#endif
