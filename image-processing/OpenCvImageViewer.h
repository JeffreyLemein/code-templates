//
// Created by jlemein on 10-10-19.
//

#pragma once

#include "ImageViewer.h"
#include <opencv2/core.hpp>

class OpenCvImageViewer : public ImageViewer<cv::Mat> {
public:
  OpenCvImageViewer(const std::string& windowName);

  void showImage(const cv::Mat &image) override;
  void showImages(const cv::Mat& m1, const cv::Mat& m2);
};
