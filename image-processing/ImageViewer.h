//
// Created by jlemein on 10-10-19.
//

#ifndef IMAGE_PROCESSING_IMAGEVIEWER_H
#define IMAGE_PROCESSING_IMAGEVIEWER_H
#include <string>

template <typename T>
class ImageViewer {
protected:
  std::string mWindowName;
public:
  ImageViewer(const std::string& windowName)
    : mWindowName(windowName) {}

  virtual void showImage(const T& image) = 0;
};
#endif // IMAGE_PROCESSING_IMAGEVIEWER_H
