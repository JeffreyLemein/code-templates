#ifndef IMAGE_PROCESSING_IMAGEOPERATION_H
#define IMAGE_PROCESSING_IMAGEOPERATION_H

#include <string>

template <typename T, typename R=T>
class ImageOperation {
public:
  virtual ~ImageOperation() = default;

  virtual void prepare() = 0;
  virtual void apply() = 0;
  virtual const R& getResult() = 0;
};

#endif // IMAGE_PROCESSING_IMAGEOPERATION_H
