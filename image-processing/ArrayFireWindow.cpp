#include "ArrayFireWindow.h"
ArrayFireWindow::ArrayFireWindow(const std::string &windowName)
: ImageViewer(windowName) {}

void ArrayFireWindow::showImage(const af::array &image) {
  af::Window window(512, 512, mWindowName.c_str());
  window.image(image);
}
