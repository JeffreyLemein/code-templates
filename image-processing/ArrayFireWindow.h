#pragma once

#include "ImageViewer.h"
#include "arrayfire.h"

class ArrayFireWindow : public ImageViewer<af::array> {
public:
  ArrayFireWindow(const std::string& windowName);

  void showImage(const af::array &image) override;
};
