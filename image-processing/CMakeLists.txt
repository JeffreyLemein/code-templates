cmake_minimum_required(VERSION 3.14)
project(image_processing)

set(CMAKE_CXX_STANDARD 17)

find_package( OpenCV 4 REQUIRED)
find_package( ArrayFire REQUIRED)


message("OpenCV include dirs: ${OpenCV_INCLUDE_DIRS}")
message("OpenCV libraries: ${OpenCV_LIBRARIES}")
message("ArrayFire include dirs: ${ArrayFire_INCLUDE_DIRS}")
message("ArrayFire libraries: ${ArrayFire_LIBRARIES}")

set(HEADER_FILES
        imageoperations/cpu/GaussianBlur.h
        imageoperations/gpu/GaussianBlur.h
        imageoperations/cpu/Laplacian.h
        imageoperations/gpu/Laplacian.h
        imageoperations/cpu/Demosaic.h
        imageoperations/gpu/Demosaic.h
        imageoperations/gpu/AddNoise.h
        imageoperations/arrayfire/GaussianBlur.h
        ImageOperation.h
        ImageViewer.h
        OpenCvImageViewer.h
        ArrayFireWindow.h)

set(SOURCE_FILES
        imageoperations/cpu/Laplacian.cpp
        imageoperations/gpu/Laplacian.cpp
        imageoperations/cpu/GaussianBlur.cpp
        imageoperations/gpu/GaussianBlur.cpp
        imageoperations/cpu/Demosaic.cpp
        imageoperations/gpu/Demosaic.cpp
        imageoperations/gpu/AddNoise.cpp
        imageoperations/arrayfire/GaussianBlur.cpp
        OpenCvImageViewer.cpp
        ArrayFireWindow.cpp)

set(LIBRARIES
        ${OpenCV_LIBRARIES}
        ArrayFire::afcuda)

#add_executable(gausspyramid gausspyramid.cpp ${RESOURCE_FILES})
#target_include_directories(gausspyramid PRIVATE ${OpenCV_INCLUDE_DIRS})
#target_link_libraries(gausspyramid ${OpenCV_LIBRARIES})

add_executable(comparison main.cpp ${SOURCE_FILES} ${HEADER_FILES} ${RESOURCE_FILES})
target_include_directories(comparison PRIVATE ${ArrayFire_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS})
target_link_libraries(comparison ${LIBRARIES})

