//
// Created by jlemein on 09-10-19.
//

#include "Laplacian.h"
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaarithm.hpp>
using namespace cv;
using namespace gpu;

Laplacian::Laplacian(const std::string &fileName) : mFileName(fileName) {
  gaussianFilter = cuda::createGaussianFilter(
      src_d.type(), src_d.type(),
      cv::Size(3,3), 0, 0,
      cv::BORDER_DEFAULT);

  laplacianFilter = cuda::createLaplacianFilter(dst_d.type(), dst_d.type(), 3);
}

void Laplacian::prepare() {
  src = imread(mFileName.c_str());
  src_d.upload(src);

  gaussianFilter = cuda::createGaussianFilter(
      src_d.type(), src_d.type(),
      cv::Size(3,3), 0, 0,
      cv::BORDER_DEFAULT);

  laplacianFilter = cuda::createLaplacianFilter(dst_d.type(), dst_d.type(), 3);
}

void Laplacian::apply() {
  static cv::cuda::GpuMat tmp_d, grayscale_d;

  gaussianFilter->apply(src_d, src_d, stream);
  cuda::cvtColor(src_d, dst_d, cv::COLOR_RGB2GRAY, 0, stream);
  laplacianFilter->apply(dst_d, dst_d, stream);
  cuda::abs(dst_d, dst_d, stream);

  stream.waitForCompletion();
}

const cv::Mat& Laplacian::getResult() {
  dst_d.download(result);
  return result;
}

