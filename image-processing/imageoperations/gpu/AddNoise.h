#pragma once

#include "../../ImageOperation.h"
#include <opencv2/core.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace gpu {
class AddNoise : public ImageOperation<cv::cuda::GpuMat, cv::Mat> {
private:
  std::string mFileName;
  cv::Mat src, dst, noise;
  cv::cuda::GpuMat src_d, dst_d, noise_d, grayscale_d;

public:
  AddNoise(const std::string& fileName);

  void prepare() override;
  void apply() override;
  const cv::Mat &getResult() override;
};
}