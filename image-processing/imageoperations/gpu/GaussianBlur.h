//
// Created by jlemein on 09-10-19.
//

#ifndef IMAGE_PROCESSING_GAUSSIANBLUR_H
#define IMAGE_PROCESSING_GAUSSIANBLUR_H

#include "../../ImageOperation.h"
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace gpu {

class GaussianBlur : public ImageOperation<cv::cuda::GpuMat, cv::Mat> {
private:
  cv::Mat src, result;
  cv::cuda::GpuMat src_d, dst_d;
  cv::Ptr<cv::cuda::Filter> mFilter;
  std::string mFileName;

public:
  GaussianBlur(const std::string &fileName);

  virtual void prepare() override;
  virtual void apply() override;
  virtual const cv::Mat& getResult() override;
};

};

#endif // IMAGE_PROCESSING_GAUSSIANBLUR_H
