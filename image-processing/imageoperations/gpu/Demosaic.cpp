#include "Demosaic.h"
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaarithm.hpp>
using namespace cv;
using namespace gpu;

Demosaic::Demosaic(const std::string& fileName)
 : mFileName(fileName) {}

void Demosaic::prepare() {
  src = imread(mFileName, CV_8UC1);
  src_d.upload(src);
}
void Demosaic::apply() {
  cuda::demosaicing(src_d, dst_d, cv::COLOR_BayerBG2RGB);
}

const cv::Mat& Demosaic::getResult() {
  dst_d.download(result);
  return result;
}