#pragma once

#include "../../ImageOperation.h"
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace gpu {

class Demosaic : public ImageOperation<cv::cuda::GpuMat, cv::Mat> {
private:
  cv::Mat src, result;
  cv::cuda::GpuMat src_d, dst_d;
  cv::cuda::Stream stream;
  std::string mFileName;

public:
  Demosaic(const std::string& fileName);

  void prepare() override;
  void apply() override;
  const cv::Mat& getResult() override;
};

}
