#include "GaussianBlur.h"
#include "opencv2/imgproc.hpp"
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudafilters.hpp>
using namespace cv;
using namespace gpu;

GaussianBlur::GaussianBlur(const std::string& fileName)
  : mFileName(fileName) {}

void GaussianBlur::prepare() {
  src = imread(mFileName.c_str());
  src_d.upload(src);
  mFilter = cuda::createGaussianFilter(
      src_d.type(), src_d.type(),
      cv::Size(3,3), 0, 0,
      cv::BORDER_DEFAULT);
}

void GaussianBlur::apply() {
  mFilter->apply(src_d, dst_d);
}

const cv::Mat& GaussianBlur::getResult() {
  dst_d.download(result);
  return result;
}