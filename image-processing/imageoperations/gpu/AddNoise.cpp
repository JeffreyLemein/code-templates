
#include "AddNoise.h"
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaarithm.hpp>

using namespace gpu;

AddNoise::AddNoise(const std::string& fileName)
  : mFileName(fileName) {}

void AddNoise::prepare() {
  src = cv::imread(mFileName);
  src_d.upload(src);

  noise = cv::Mat(src.size(), src.type());

  // this is not entirely gaussian noise, since only positive values are stored
  cv::randn(noise,  cv::Scalar::all(0.0), cv::Scalar::all(120.0));

  noise_d.upload(noise);

}

void AddNoise::apply() {
  cv::cuda::add(src_d, noise_d, dst_d);
}

const cv::Mat& AddNoise::getResult() {
  dst_d.download(dst);
  return dst;
}
