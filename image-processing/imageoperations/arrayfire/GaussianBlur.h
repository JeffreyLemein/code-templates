#pragma once

#include "../../ImageOperation.h"
#include <arrayfire.h>
namespace arrayfire {

class GaussianBlur : public ImageOperation<af::array> {
private:
  af::array src, result;
  const std::string mFileName;

public:
  GaussianBlur(const std::string& fileName);

  void prepare() override;
  void apply() override;
  const af::array &getResult() override;
};

}
