#include "GaussianBlur.h"
using namespace arrayfire;

arrayfire::GaussianBlur::GaussianBlur(const std::string &fileName)
    : mFileName(fileName) {}

void arrayfire::GaussianBlur::prepare() {
  src = af::loadImage(mFileName.c_str());
}
void arrayfire::GaussianBlur::apply() {}
const af::array &arrayfire::GaussianBlur::getResult() {
  return result;
}

