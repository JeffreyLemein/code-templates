#pragma once

#include "../../ImageOperation.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace cpu {
class GaussianBlur : public ImageOperation<cv::Mat> {
private:
  cv::Mat src, result;
  std::string mFileName;

public:
  GaussianBlur(const std::string &fileName);

  virtual void prepare() override;
  virtual void apply() override;
  virtual const cv::Mat& getResult() override;

};
}
