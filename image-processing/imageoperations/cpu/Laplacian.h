#pragma once

#include "../../ImageOperation.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace cpu {
class Laplacian : public ImageOperation<cv::Mat> {
private:
  cv::Mat src, result;
  std::string mFileName;

public:
  Laplacian(const std::string &fileName);

  virtual void prepare() override;
  virtual void apply() override ;
  virtual const cv::Mat& getResult() override ;
};
};
