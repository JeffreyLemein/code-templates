#include "GaussianBlur.h"
using namespace cpu;

GaussianBlur::GaussianBlur(const std::string& fileName)
: mFileName(fileName) {}

void GaussianBlur::prepare() {
  src = cv::imread(mFileName);
}

void GaussianBlur::apply() {
  static cv::Mat gauss, gaussGray;

  cv::GaussianBlur(src, gauss, cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT);
  cv::cvtColor(gauss, gaussGray, cv::COLOR_BGR2GRAY);

  cv::Laplacian(gaussGray, result, CV_16S, 3);
  cv::convertScaleAbs(result, result);
}

const cv::Mat& GaussianBlur::getResult() {
  return result;
}