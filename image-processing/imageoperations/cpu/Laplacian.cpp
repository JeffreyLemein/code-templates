#include "Laplacian.h"
using namespace cpu;

Laplacian::Laplacian(const std::string& fileName)
  : mFileName(fileName) {}

void Laplacian::prepare() {
  src = cv::imread(mFileName);
}
void Laplacian::apply() {
  static cv::Mat gauss, gaussGray;

  cv::GaussianBlur(src, gauss, cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT);
  cv::cvtColor(gauss, gaussGray, cv::COLOR_BGR2GRAY);

  cv::Laplacian(gaussGray, result, CV_16S, 3);
  cv::convertScaleAbs(result, result);
}

const cv::Mat& Laplacian::getResult() {
  return result;
}