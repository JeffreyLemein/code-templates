#include "Demosaic.h"
using namespace cv;
using namespace cpu;

Demosaic::Demosaic(const std::string &fileName)
: mFileName(fileName) {}

void Demosaic::prepare() {
  src = cv::imread(mFileName, CV_8UC1);
}

void Demosaic::apply() {
  cv::demosaicing(src, result, cv::COLOR_BayerBG2RGB);
}
const cv::Mat& Demosaic::getResult()  {
  return result;
}