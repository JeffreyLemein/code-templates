//
// Created by jlemein on 10-10-19.
//

#include "OpenCvImageViewer.h"
#include <cstdarg>
#include <opencv2/highgui.hpp>
using namespace cv;

OpenCvImageViewer::OpenCvImageViewer(const std::string& windowName)
    : ImageViewer(windowName) {}

void OpenCvImageViewer::showImage(const cv::Mat &image) {
  cv::namedWindow(mWindowName, cv::WINDOW_NORMAL | cv::WINDOW_KEEPRATIO | cv::WINDOW_GUI_EXPANDED);
  cv::resizeWindow(mWindowName, 1200, 800);
  cv::imshow(mWindowName, image);

  cv::waitKey(0);
}

void OpenCvImageViewer::showImages(const cv::Mat& m1, const cv::Mat& m2) {
  int cols = m1.rows + m2.rows;
  int rows = m1.rows + m2.rows;
  Mat result(Size(cols, rows), m1.type(), Scalar::all(0));

  Mat roi1 = result(Rect(0, 0, m1.cols, m1.rows));
  Mat roi2 = result(Rect(m1.cols, m1.rows, m2.cols, m2.rows));
  m1.copyTo(roi1);
  m2.copyTo(roi2);

  showImage(result);
}