#include <iostream>
#include <string>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

const string WINDOW_NAME = "HDR Processing";
const string WINDOW_NAME2 = "OpenCV";

void gaussPyramid(unsigned int k, const Mat& src, Mat& dst) {
    dst = src;
    for (int i=0; i<k; ++i) {
        pyrDown(dst, dst);
    }
    for (int i=0; i<k; ++i) {
        pyrUp(dst, dst);
    }
}

void laplacian(unsigned int k, const Mat& src, Mat& dst) {
    gaussPyramid(k, src, dst);

    Mat dstGray, srcGray;
    cvtColor(src, srcGray, cv::COLOR_BGR2GRAY);
    cvtColor(dst, dstGray, cv::COLOR_BGR2GRAY);
    absdiff(srcGray, dstGray, dst);
}

int main(int argc, char** argv) {

    string imageName = "../res/manor/manor_normal.jpg";
    Mat source = imread( imageName, IMREAD_COLOR );

    if(source.empty()) {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    /*
     * My way of computing laplace
     */
    {
        Mat laplace;
        laplacian(3, source, laplace);

        namedWindow(WINDOW_NAME, WINDOW_NORMAL | WINDOW_KEEPRATIO | WINDOW_GUI_EXPANDED);
        resizeWindow(WINDOW_NAME, 1200, 800);
        imshow(WINDOW_NAME, laplace);
    }

    /*
     * OpenCV way of computing laplace
     */
    {
        Mat gaussian, gaussianGray, laplace2;

        GaussianBlur(source, gaussian, Size(3, 3), 0, 0, BORDER_DEFAULT);
        cvtColor(gaussian, gaussianGray, cv::COLOR_BGR2GRAY);
        Laplacian(gaussianGray, laplace2, CV_16S, 3);
        convertScaleAbs(laplace2, laplace2);

        namedWindow(WINDOW_NAME2, WINDOW_NORMAL | WINDOW_KEEPRATIO | WINDOW_GUI_EXPANDED);
        resizeWindow(WINDOW_NAME2, 1200, 800);
        imshow(WINDOW_NAME2, laplace2);
    }



    waitKey(0);
    return 0;
}