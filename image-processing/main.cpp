#include "OpenCvImageViewer.h"
#include "ArrayFireWindow.h"

#include "imageoperations/cpu/Demosaic.h"
#include "imageoperations/cpu/GaussianBlur.h"
#include "imageoperations/cpu/Laplacian.h"
#include "imageoperations/gpu/AddNoise.h"
#include "imageoperations/gpu/Demosaic.h"
#include "imageoperations/gpu/GaussianBlur.h"
#include "imageoperations/gpu/Laplacian.h"
#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

inline double diffMilliseconds(const timespec& start, const timespec& end) {
  double secs = static_cast<double>(end.tv_sec - start.tv_sec) * 1000.0;
  double millisecs = static_cast<double>(end.tv_nsec - start.tv_nsec) / static_cast<double>(1e6);
  return secs + millisecs;
}

template<typename T, typename R>
double measurePerformance(shared_ptr<ImageOperation<T, R>> operation) {
  static struct timespec tStart, tEnd;
  clock_gettime(CLOCK_REALTIME, &tStart);
  operation->apply();
  clock_gettime(CLOCK_REALTIME, &tEnd);

  return diffMilliseconds(tStart, tEnd);
}

int main(int argc, char** argv) {
  using TestCase = tuple<std::string,
      shared_ptr<ImageOperation<cv::Mat>>,
      shared_ptr<ImageOperation<cv::cuda::GpuMat, cv::Mat>>>;

  struct timespec startCpu, startGpu, endCpu, endGpu;

  printf("OpenCV 4.1.1\n"
         "=============================================================================\n");

  string fileName = "../res/manor/manor_normal.jpg";
  string bayerFileName = "../res/bayer.png";

  vector<TestCase> comparisons = {
      {std::string("-- Dummy Run --"), make_shared<cpu::Laplacian>(fileName), make_shared<gpu::Laplacian>(fileName)},
      {std::string("Laplacian      "), make_shared<cpu::Laplacian>(fileName), make_shared<gpu::Laplacian>(fileName)},
      {std::string("Demosaic       "), make_shared<cpu::Demosaic>(bayerFileName), make_shared<gpu::Demosaic>(bayerFileName)},
      {std::string("Gaussian Blur  "), make_shared<cpu::GaussianBlur>(fileName), make_shared<gpu::GaussianBlur>(fileName)},
      {std::string("Noise          "), make_shared<cpu::GaussianBlur>(fileName), make_shared<gpu::AddNoise>(fileName)}
  };

  for (auto& testCase : comparisons) {
    auto name = get<0>(testCase);
    auto gpuOperation = get<2>(testCase);
    auto cpuOperation = get<1>(testCase);

    cpuOperation->prepare();
    gpuOperation->prepare();

    double timeCpu = measurePerformance(cpuOperation);
    double timeGpu = measurePerformance(gpuOperation);

    cout << name << ":\t CPU: " << timeCpu << " millisecs,\t GPU: " << timeGpu << " millisecs\n";
  }

  int index = 3;
  auto windowName = std::get<0>(comparisons[index]);
  const auto& cpuImage = std::get<1>(comparisons[index])->getResult();
  const auto& gpuImage = std::get<2>(comparisons[index])->getResult();

  OpenCvImageViewer viewer(windowName);
  viewer.showImage(gpuImage);

  ArrayFireWindow window("Hello ArrayFire");
  float kernel[9] = {1, 0, 1, 0, 0, 0, 1, 1, 0};
  af::array image = af::array(3,3, kernel);//();//= af::loadImage(fileName.c_str(), true);

  window.showImage(image);

  return 0;
}
