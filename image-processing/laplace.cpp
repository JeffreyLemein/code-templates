//
// Created by jlemein on 04-10-19.
//
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
using namespace cv;
using namespace std;

int main() {

  Mat src = imread("../manor/manor-normal.jpg");
  Mat srcGray;

  GaussianBlur(src, src, Size(3,3), 0, 0, BORDER_DEFAULT);
  cvtColor(src, srcGray, COLOR_BGR2GRAY);

  // Gradient X
  int ddepth = CV_16S;  // depth of the output image
  Mat gradX, gradY;
  Sobel(srcGray, gradX, ddepth, 1, 0);
  Sobel(srcGray, gradY, ddepth, 0, 1);

  //Mat absGrade
}