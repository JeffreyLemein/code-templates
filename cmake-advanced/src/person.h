#ifndef PERSON_H
#define PERSON_H

/**
 * @brief The Person class represents a person
 */
class Person
{
public:
    Person();

    //!! @brief this functions says something
    void saySomething();
};

#endif // PERSON_H
