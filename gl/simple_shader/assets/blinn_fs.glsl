#version 430

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;

//in VertexData {
//    vec2 texCoord;
//    vec3 normal;
//};

layout(location=0) out vec4 colorOut;

void main() {
    colorOut = vec4(0.0, 0.0, 0.0, 1.0);
}
