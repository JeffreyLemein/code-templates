#version 430

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;

uniform vec3 lightPosition;
uniform vec3 lightDiffuse;

layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;
layout (location=2) in vec2 texCoord;

//out VertexData {
//    vec2 texCoord;
//    vec3 normal;
//} VertexOut;

void main() {
  //  VertexOut.texCoord = texCoord;
    //VertexOut.normal = normal;
    mat4 MVP = perspective * view * model;
    gl_Position = MVP * vec4(position, 1.0);
}
