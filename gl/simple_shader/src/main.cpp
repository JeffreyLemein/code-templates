#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "plyloader.h"
#include "ShaderSource.h"
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace std;

const GLfloat vertices[] = {0.0f, 0.0f, 1.0f,
    1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f};
const GLushort indices[] = {2, 1, 0};

PlyLoader* loader;
unsigned int vbo, ibo;
GLuint vertexShader, fragmentShader, program;
char* buffer[1000];

/* Light position */
glm::vec3 lightPosition(-10.0f, 10.0f, 10.0f), diffuseColor(1.0f, 1.0f, 1.0f);

/* Positioning */
const int WINDOW_WIDTH = 800, WINDOW_HEIGHT = 600;
float fovy = 120, aspect = WINDOW_WIDTH / static_cast<float> (WINDOW_HEIGHT), zNear = 0.1f, zFar = 500.0f;
glm::vec3 eye(2.0f, 2.0f, 2.0f), center(.0f, .0f, .0f), up(.0f, 1.0f, .0f);
glm::mat4 model = glm::mat4(1.0f);
glm::mat4 view = glm::lookAt(eye, center, up);
glm::mat4 perspective = glm::perspective(fovy, aspect, zNear, zFar);

void compileShader(const std::string& fileName, unsigned int shader) {
    std::vector<char*> sourceLines;
    int lineCount;

    ShaderSource::Source(fileName, sourceLines, lineCount);
    glShaderSource(shader, lineCount, &sourceLines[0], NULL);
    glCompileShader(shader);

    GLint compiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled) {
        cout << "Shader '" + fileName + "' OK" << endl;
    } else {
        cout << "Shader '" << fileName << "' failed to compile:" << endl;
        GLint logSize = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
        GLchar* infoLog = new GLchar[logSize];
        glGetShaderInfoLog(shader, logSize, NULL, infoLog);
        cout << infoLog << endl;
        delete[] infoLog;
    }
}

void linkProgram(int program) {
    glLinkProgram(program);
    GLint isLinked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE) {
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        std::vector<GLchar> infoLog(maxLength);
        glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

        cout << "Failed to link" << endl;
        cout << "Info log size: " << infoLog.size() << endl;
        for (auto line : infoLog) {
            cout << line << endl;
        }

        // The program is useless now. So delete it.
        glDeleteProgram(program);
    } else {
        cout << "Linking OK" << endl;
    }
}

void init() {
    glClearColor(0.4f, 0.3f, 0.2f, 1.0f);

    /* Set up vertex buffer */
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ibo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    for (int i = 0; i < 12; ++i) {
        cout << i << ": " << loader->getVertices()[i] << endl;
    }
    glBufferData(GL_ARRAY_BUFFER, loader->getVertexCount() * 6 * sizeof (GLfloat), (void*) loader->getVertices(), GL_STATIC_DRAW);
    //glBufferData(GL_ARRAY_BUFFER, 9 * sizeof (GLfloat), (void*) vertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    //glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3, 0); // for triangle
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof (GLfloat), (void*) (0 * sizeof (GLfloat)));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof (GLfloat), (void*) (3 * sizeof (GLfloat)));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, loader->getIndexCount() *3 * sizeof (unsigned short), (void*) loader->getIndices(), GL_STATIC_DRAW);
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof (GLushort), (void*) indices, GL_STATIC_DRAW);

    /* Set up render program */
    program = glCreateProgram();
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    compileShader("assets/blinn_vs.glsl", vertexShader);
    compileShader("assets/blinn_fs.glsl", fragmentShader);

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    linkProgram(program);
}

void render() {
    GLint location;

    //draw wireframe
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glUseProgram(program);
    location = glGetUniformLocation(program, "model");

    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(model));

    location = glGetUniformLocation(program, "view");
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(view));

    location = glGetUniformLocation(program, "perspective");
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(perspective));

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glDrawElements(GL_TRIANGLES, loader->getTriangleCount()*3, GL_UNSIGNED_SHORT, (void*) 0);
}

int main(void) {
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    loader = new PlyLoader("assets/icosphere.ply");
    loader->parse();


    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Hello World", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    if (GLEW_OK != glewInit()) {
        cout << "GLEW Error" << endl;
        return -1;
    }

    init();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /* Render here*/
        render();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
