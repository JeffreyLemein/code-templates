#version 430

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;

uniform vec4 lightPosition;
uniform vec3 lightDiffuse;
uniform int lightIntensity;

uniform vec3 offsets[100];
uniform vec3 colors[100];

layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;
layout (location=2) in vec2 texCoord;

out VS_Out {
    vec2 texCoord;
    vec3 normal;
    vec4 lightVector;
    vec3 eyeVector;
    vec3 color;
} vsOut;

void main() {
    vec3 offset = offsets[gl_InstanceID];
    vsOut.color = colors[gl_InstanceID];

    vec4 _positionWorld = model * vec4(position+offset, 1.0f);
    vec4 _positionModel = view * model * vec4(position, 1.0f);
    mat4 MV = view * model;

    if (lightPosition.w > 0.0f) {
        // point light
        vsOut.lightVector = lightPosition - _positionWorld;
        vsOut.lightVector.w = 1.0f;
    } else {
        // directional light
        vsOut.lightVector = lightPosition;
    }
    vsOut.eyeVector = -_positionModel.xyz;
    
    //VertexOut.texCoord = texCoord;
    vsOut.normal = (model * vec4(normal, 0.0)).xyz;
    mat4 MVP = perspective * view * model;

    gl_Position = MVP * vec4(position + offset, 1.0);
}
