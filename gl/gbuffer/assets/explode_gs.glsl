/*
 * Sample dummy shader to check the highlighter plugin.
 */

#version 430

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

// From vertex shader
in VS_Out {
    vec2 texCoord;
    vec3 normal;
    vec4 lightVector;
    vec3 eyeVector;
} gsIn[];


// Geometry Out
out GS_Out {
    vec2 texCoord;
    vec3 normal;
    vec4 lightVector;
    vec3 eyeVector;
} gsOut;

void main() {
    gsOut.lightVector = gsIn[0].lightVector;
    gsOut.eyeVector = gsIn[0].eyeVector;

    gsOut.texCoord = gsIn[0].texCoord;
    gsOut.normal = gsIn[0].normal;
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    gsOut.lightVector = gsIn[1].lightVector;
    gsOut.eyeVector = gsIn[1].eyeVector;
    gsOut.texCoord = gsIn[1].texCoord;
    gsOut.normal = gsIn[1].normal;
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    gsOut.lightVector = gsIn[2].lightVector;
    gsOut.eyeVector = gsIn[2].eyeVector;
    gsOut.texCoord = gsIn[2].texCoord;
    gsOut.normal = gsIn[2].normal;
    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    EndPrimitive();
}
