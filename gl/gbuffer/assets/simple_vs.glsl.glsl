#version 430

layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;

out VertexOut {
    vec3 color;
} vsOut;

void main() {
    vsOut.color = color;
    gl_Position = position;
}
