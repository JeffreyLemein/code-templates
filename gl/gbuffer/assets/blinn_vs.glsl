#version 430

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;
uniform vec3 color;

uniform vec4 lightPosition; //world space
uniform vec3 lightDiffuse;
uniform int lightIntensity;

layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;
layout (location=2) in vec2 texCoord;

out VS_Out {
    vec2 texCoord;
    vec3 normal;
    vec4 lightVector;
    vec3 eyeVector;
} vsOut;

void main() {
    vec4 _positionWorld = model * vec4(position, 1.0);
    vec4 _positionModel = view * model * vec4(position, 1.0);
    mat4 MV = view * model;
    
    if (lightPosition.w > 0.0f) {
        // point light
        vsOut.lightVector = lightPosition - _positionWorld;
        vsOut.lightVector.w = 1.0f;
    }  else {
        // directional light
        vsOut.lightVector = lightPosition;
    }
    vsOut.eyeVector = -_positionModel.xyz;
    
    //VertexOut.texCoord = texCoord;
    vsOut.normal = (model * vec4(normal, 0.0)).xyz;
    mat4 MVP = perspective * view * model;

    gl_Position = MVP * vec4(position, 1.0);
}
