#version 430

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;
uniform vec3 color;

uniform vec4 lightPosition;
uniform vec3 lightDiffuse;
uniform float lightIntensity;

uniform float specularIntensity;
uniform float shininess;

// Geometry Out
/*in GS_Out {
    vec2 texCoord;
    vec3 normal;
    vec3 lightVector;
    vec3 eyeVector;
} fsIn;*/

in VS_Out {
    vec2 texCoord;
    vec3 normal;
    vec4 lightVector;
    vec3 eyeVector;
} fsIn;


in vec3 lightVector;
in vec3 eyeVector;

layout(location=0) out vec4 colorOut;

void main() {
    vec3 lightDirection = normalize(fsIn.lightVector.xyz);

    vec3 H = normalize(lightDirection + normalize(fsIn.eyeVector));
    float NdotL = dot(normalize(fsIn.normal), lightDirection);
    float diffuseIntensity = clamp(NdotL, 0.0f, 1.0f);
    vec3 outDiffuse = (diffuseIntensity * color * lightDiffuse);// / (lightDistance * lightDistance);

    float lightAttenuation = 1.0f;
    if (fsIn.lightVector.w > 0.0f) {
        float lightDistance = length(fsIn.lightVector.xyz);
        lightAttenuation = lightIntensity / (lightDistance * lightDistance);
        //outDiffuse /= (lightDistance * lightDistance);
    }

    float NdotH = dot(normalize(fsIn.normal), H);    
    vec3 outSpecular = specularIntensity * pow(clamp(NdotH, 0.0f, 1.0f), shininess) * vec3(1,1,1);/// lightDistance;
    
    colorOut = vec4(lightAttenuation * (outDiffuse + outSpecular), 1.0);// vec4(outDiffuse, 1.0);
}
