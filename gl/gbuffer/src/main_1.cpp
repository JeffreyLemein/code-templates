#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "plyloader.h"
#include "ShaderSource.h"
#include "SimpleGlUtil.h"
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <sstream>
using namespace std;

const char* windowTitle = "Instanced Rendering Spheres";
const int WINDOW_WIDTH = 1024, WINDOW_HEIGHT = 768;

inline void checkError() {
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
        cout << "OpenGL Error: " << err << endl;
        exit(1);
    }
}

/* Ground plane */
const GLfloat planeVertices[] = {
    -1.0f, 0.0f, -1.0f, .0f, 1.0f, .0f,
    1.0f, 0.0f, -1.0f, .0f, 1.0f, .0f,
    1.0f, 0.0f, 1.0f, .0f, 1.0f, .0f,
    -1.0f, 0.0f, 1.0f, .0f, 1.0f, .0f
};
const GLushort planeIndices[] = {0, 1, 2, 0, 2, 3};

PlyLoader* loader;
GLuint floorProgram, sphereProgram;
RenderState planeRs, icoSphereRs;

glm::mat4 planeModel = glm::scale(glm::mat4(1.0f), glm::vec3(20.0f, 20.0f, 20.0f));
glm::vec3 grayColor = glm::vec3(0.2f, 0.2f, 0.2f);
glm::vec3 blueColor = glm::vec3(0.1f, 0.2, 0.8f);
float floorShininess = 32.0f;
float sphereShininess = 32.0f;
/* End plane */

/* Instances of sphere */
std::vector<glm::vec3> sphereOffsets, sphereColors;

/* Light position */
glm::vec4 lightPosition(-0.5f, 5.0f, 0.0f, 0.0f);
glm::vec3 lightDiffuseColor(1.0f, 1.0f, 1.0f);
float lightIntensity = 1.0f;
float lightSpecularIntensity = 1.0f;
float lightSpecularPower = 3.0f;

/* Positioning */

float fovy = 120, aspect = WINDOW_WIDTH / static_cast<float> (WINDOW_HEIGHT), zNear = 0.1f, zFar = 500.0f;
glm::vec3 eye(12.0f, 8.0f, 12.0f), center(.0f, 1.0f, .0f), up(.0f, 1.0f, .0f);
glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
glm::mat4 view = glm::lookAt(eye, center, up);
glm::mat4 perspective = glm::perspective(fovy, aspect, zNear, zFar);

void initInstances();

void init() {
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    /* Create plane buffers */
    planeRs = SimpleGlUtil::createBufferWithData(planeVertices, sizeof (planeVertices), planeIndices, sizeof (planeIndices));

    loader = new PlyLoader("assets/icosphere.ply");
    loader->parse();
    icoSphereRs = SimpleGlUtil::createBufferWithData(loader->getVertices(), loader->getVerticesSizeBytes(), loader->getIndices(), loader->getIndicesSizeBytes());

    sphereProgram = SimpleGlUtil::createShaderProgram("assets/blinn_instanced_vs.glsl", "assets/blinn_instanced_fs.glsl");
    floorProgram = SimpleGlUtil::createShaderProgram("assets/blinn_vs.glsl", "assets/blinn_fs.glsl");

    initInstances();
}

void initInstances() {
    sphereOffsets.reserve(100);
    sphereColors.reserve(100);

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            float x = -12.5f + i * 2.5f;
            float z = -12.5f + j * 2.5f;
            sphereOffsets.push_back(glm::vec3(x, 0.0f, z));

            float r = rand() / static_cast<float> (RAND_MAX);
            float g = rand() / static_cast<float> (RAND_MAX);
            float b = rand() / static_cast<float> (RAND_MAX);
            sphereColors.push_back(glm::vec3(r, g, b));
        }
    }

    //sphereOffsets[0] = glm::vec3(0.0f, 0.0f, 0.0f);
    cout << "Sphere offsets: " << sphereOffsets.size() << endl;

    glUseProgram(sphereProgram);
    SimpleGlUtil::setUniform(sphereProgram, "offsets", &sphereOffsets[0], sphereOffsets.size());
    SimpleGlUtil::setUniform(sphereProgram, "colors", &sphereColors[0], sphereColors.size());
}

void render() {
    checkError();
    GLint location;

    /* Render shaded */
    glShadeModel(GL_SMOOTH);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    /* Setup shader program */
    glUseProgram(floorProgram);
    SimpleGlUtil::setUniform(floorProgram, "lightDiffuse", lightDiffuseColor);
    SimpleGlUtil::setUniform(floorProgram, "lightPosition", lightPosition);
    SimpleGlUtil::setUniform(floorProgram, "lightIntensity", lightIntensity);
    SimpleGlUtil::setUniform(floorProgram, "shininess", floorShininess);
    SimpleGlUtil::setUniform(floorProgram, "specularIntensity", lightSpecularIntensity);
    SimpleGlUtil::setUniform(floorProgram, "view", view);
    SimpleGlUtil::setUniform(floorProgram, "perspective", perspective);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    /* Render plane */
    SimpleGlUtil::setUniform(floorProgram, "color", grayColor);
    SimpleGlUtil::setUniform(floorProgram, "model", planeModel);
    SimpleGlUtil::setRenderState(planeRs);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, (void*) 0);

    /* Render ico sphere */
    glUseProgram(sphereProgram);
    SimpleGlUtil::setRenderState(icoSphereRs);
    SimpleGlUtil::setUniform(sphereProgram, "lightDiffuse", lightDiffuseColor);
    SimpleGlUtil::setUniform(sphereProgram, "lightPosition", lightPosition);
    SimpleGlUtil::setUniform(sphereProgram, "lightIntensity", lightIntensity);
    SimpleGlUtil::setUniform(sphereProgram, "shininess", sphereShininess);
    SimpleGlUtil::setUniform(sphereProgram, "specularIntensity", lightSpecularIntensity);
    SimpleGlUtil::setUniform(sphereProgram, "view", view);
    SimpleGlUtil::setUniform(sphereProgram, "perspective", perspective);
    SimpleGlUtil::setUniform(sphereProgram, "model", model);
    glDrawElementsInstanced(GL_TRIANGLES, loader->getTriangleCount()*3, GL_UNSIGNED_SHORT, (void*) 0, sphereOffsets.size());
    //glDrawElements(GL_TRIANGLES, loader->getTriangleCount()*3, GL_UNSIGNED_SHORT, (void*) 0);
}

int main(void) {
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    //glfwWindowHint(GLFW_SAMPLES);
    glfwWindowHint(GLFW_SAMPLES, 16);
    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, windowTitle, NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    if (GLEW_OK != glewInit()) {
        cout << "GLEW Error" << endl;
        return -1;
    }

    init();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /* Render here*/
        render();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    delete loader;

    glfwTerminate();
    return 0;
}
