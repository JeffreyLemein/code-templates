/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   plyloader.h
 * Author: jeffrey
 *
 * Created on October 9, 2018, 10:14 PM
 */

#ifndef PLYLOADER_H
#define PLYLOADER_H

#include <string>

class PlyLoader {
private:
    std::string mFileName;
    float* mVertices = 0;
    unsigned short* mTriangles = 0;
    int mVertexCount, mFaceCount;
    
public:
    PlyLoader(const std::string& fileName);
    void parse();
    
    /**
     * Gets the vertices of the ply model
     * @return point to beginning of vertex array
     */
    const float* getVertices() const;
    const unsigned short* getIndices() const;
    int getVertexCount() const;
    int getTriangleCount() const;
    int getIndexCount() const;
    
    int getVerticesSizeBytes() const;
    int getIndicesSizeBytes() const;
   
};

#endif /* PLYLOADER_H */

