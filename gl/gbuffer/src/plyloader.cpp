/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "plyloader.h"
#include <fstream>
#include <iostream>
#include <sstream>

PlyLoader::PlyLoader(const std::string& fileName) : mFileName(fileName) {

}

int PlyLoader::getIndexCount() const {
    return mFaceCount * 3;
}

int PlyLoader::getVertexCount() const {
    return mVertexCount;
}

int PlyLoader::getTriangleCount() const {
    return mFaceCount;
}

const unsigned short* PlyLoader::getIndices() const {
    return mTriangles;
}

const float* PlyLoader::getVertices() const {
    return mVertices;
}

int PlyLoader::getVerticesSizeBytes() const {
    return mVertexCount * sizeof (float) * 6;
}

int PlyLoader::getIndicesSizeBytes() const {
    return mFaceCount * 3 * sizeof (unsigned short);
}

void PlyLoader::parse() {
    std::ifstream input(mFileName.c_str());
    if (input.fail()) {
        std::cout << "Failed to parse file: " << mFileName << std::endl;
    }

    //  int mFileType;
    std::string key, formatType;

    std::string fileType, fileFormat;
    std::string elementType;
    int vertexCount, faceCount;
    std::string line;

    while (!input.eof()) {
        getline(input, line);
        std::istringstream iss(line);

        iss >> key;
        if (key == "ply") {
            std::cout << "Confirmed PLY format" << std::endl;
        }
        if (key == "comment") {
            continue;
        }
        if (key == "format") {
            std::string value;
            iss >> value;
            std::cout << "Value" << value << std::endl;
        }
        if (key == "element") {
            iss >> elementType;
            if (elementType == "vertex") {
                iss >> this->mVertexCount;
            }
            if (elementType == "face") {
                iss >> this->mFaceCount;
            }
            //readVertexProperties()

        }

        if (key == "end_header") {
            std::cout << "End of header found" << std::endl;
            break;
        }

    }

    std::cout << "VertexCount: " << mVertexCount << ", FaceCount: " << mFaceCount << std::endl;

    mVertices = new float[mVertexCount * 6];
    float x, y, z, nx, ny, nz;
    unsigned short n, a, b, c;

    for (int i = 0; i < mVertexCount; ++i) {
        getline(input, line);
        std::istringstream iss(line);

        iss >> x >> y >> z >> nx >> ny >> nz;
        mVertices[i * 6] = x;
        mVertices[i * 6 + 1] = y;
        mVertices[i * 6 + 2] = z;
        mVertices[i * 6 + 3] = nx;
        mVertices[i * 6 + 4] = ny;
        mVertices[i * 6 + 5] = nz;
        //std::cout << "Reading vertices" << std::endl;
    }

    mTriangles = new unsigned short[mFaceCount * 3];

    for (int i = 0; i < mFaceCount; ++i) {
        getline(input, line);
        std::istringstream iss(line);

        iss >> n >> a >> b >> c;
        mTriangles[i * 3] = a;
        mTriangles[i * 3 + 1] = b;
        mTriangles[i * 3 + 2] = c;
        //std::cout << "Reading triangles";
    }

    std::cout << "Done" << std::endl;

}
