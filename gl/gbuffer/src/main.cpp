#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "plyloader.h"
#include "ShaderSource.h"
#include "SimpleGlUtil.h"
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <sstream>
using namespace std;

const char* windowTitle = "Instanced Rendering Spheres";
const int WINDOW_WIDTH = 1024, WINDOW_HEIGHT = 768;

inline void checkError() {
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
        cout << "OpenGL Error: " << err << endl;
        exit(1);
    }
}

// Triangle vertices and color
const GLfloat triangle[] = {
    -.5f, .0f, .0f, 1.0f, 0.0f, 0.0f,
    .5f, .0f, -0.0f, .0f, 1.0f, 0.0f,
    .0f, 1.0f, .0f, .0f, 1.0f, .0f,
};

GLuint shaderProgram;
RenderState planeRs, icoSphereRs;

glm::mat4 triangleModel = glm::scale(glm::mat4(1.0f), glm::vec3(20.0f, 20.0f, 20.0f));


/* Instances of sphere */
std::vector<glm::vec3> triangleOffsets; //, triangleColors;

// camera
float fovy = 120;
float aspect = WINDOW_WIDTH / static_cast<float> (WINDOW_HEIGHT);
float zNear = 0.1f;
float zFar = 500.0f;

glm::vec3 eye(12.0f, 8.0f, 12.0f);
glm::vec3 center(.0f, 1.0f, .0f);
glm::vec3 up(.0f, 1.0f, .0f);

glm::mat4 model = glm::mat4(1.0f); //glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f));
glm::mat4 view = glm::lookAt(eye, center, up);
glm::mat4 perspective = glm::perspective(fovy, aspect, zNear, zFar);

void createShaderProgram(const char* vsFileName, const char* fsFileName);
void initShaderParameters();

void init() {
    glEnable(GL_DEPTH_TEST | GL_DOUBLEBUFFER);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /* Create plane buffers */
    glGenBuffers(&vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof (triangle), triangle, GL_STATIC_DRAW);

    shaderProgram = createShaderProgram("assets/instanced_vs.glsl", "assets/instanced_fs.glsl");

    initShaderParameters();
}

void createShaderProgram(const char* vsFileName, const char* fsFileName) {

}

void initShaderParameters() {
    triangleOffsets.reserve(100);

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            float x = -12.5f + i * 2.5f;
            float z = -12.5f + j * 2.5f;
            sphereOffsets.push_back(glm::vec3(x, 0.0f, z));
        }
    }

    glUseProgram(sphereProgram);
    SimpleGlUtil::setUniform(sphereProgram, "offsets", &sphereOffsets[0], sphereOffsets.size());
}

void render() {
    /* Setup shader program */
    glUseProgram(shaderProgram);
    SimpleGlUtil::setUniform(floorProgram, "view", view);
    SimpleGlUtil::setUniform(floorProgram, "perspective", perspective);
    SimpleGlUtil::setUniform(sphereProgram, "model", model);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    /* Render plane */
    glDrawArraysInstanced(GL_TRIANGLES, 3, GL_FLOAT, (void*) 0, triangleOffsets.size());
}

int main(void) {
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    glfwWindowHint(GLFW_SAMPLES, 16);
    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, windowTitle, NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    if (GLEW_OK != glewInit()) {
        cout << "GLEW Error" << endl;
        return -1;
    }

    init();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /* Render here*/
        render();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    delete loader;

    glfwTerminate();
    return 0;
}
